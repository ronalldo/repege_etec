#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "useful.h"
#include "cutscenes.h"
#include "map_maker.h"
#include "characters.h"

 // ovo faze repege da etec kskksksksksskk
void main(){
	char decisao, sexo, nome[25];
	int cont_decisao, exit;
	
	cont_decisao = 0; // completamente desnecessario
	exit = 0;	

	
	
	//criando herois
	//        HP   Atq Def Hit lv esp exp expL id dead? name     sprite
	createPer(100, 35, 10, 50, 1, 0,  0,  40,  0,  0,  "Heitor", "o/");
		
	createPer(100, 40, 40, 80, 1, 0, 0, 40, 1, 0, "Victor", "O");

	createPer(100, 40, 20, 80, 1, 0, 0, 40, 2, 0, "NPC", "o");
	
	createPer(100, 40, 20, 80, 1, 0, 0, 40, 3, 0, "NPC 2: Electic Boogaloo", "<o");
	
	struct party herois;
	herois.membros[0] = per[0].id;
	herois.membros[1] = per[1].id;
	herois.isDefeated = 0;
	herois.membrosUp = sizeArrayN(herois.membros);
	
	struct party viloes;
	viloes.membros[0] = per[2].id;
	viloes.membros[1] = per[3].id;
	viloes.isDefeated = 0;
	viloes.membrosUp = sizeArrayN(viloes.membros);

	printf(" _____ _____ _____ _____\n");
	printf("|  ___|_   _|  ___/  __ \\\n");
	printf("| |__   | | | |__ | /  \\/\n");
	printf("|  __|  | | |  __|| |\n");
	printf("| |___  | | | |___| \\__/\\\n");
	printf("\\____/  \\_/ \\____/ \\____/\n");
	
	
	printf("p: jogar, l: load\n");
	while(exit == 0){ 
		scanf(" %c", &decisao);
	
		switch(decisao){
			case 'p':
			case 'P':	
				printf("Qual é o seu nome? ");
				scanf(" %s", nome);
			
				printf("Qual é o seu sexo? [m/f]");
				scanf(" %c", &sexo);
			
				prossiga();
				showCutscene(0, nome, sexo);
				printf("Matheus: A batalha a segir e apenas um teste e nao tem haver com a historia\n\t(o.o)");
				prossiga();
				battle(herois, viloes);
				prossiga();
				prossiga();
			
				printf("Matheus: Pera lá meu consagrado, eu ainda não fiz o resto do jogo (e você perdeu)\n");
				printf("\t(o.o)\n");
				prossiga();
				exit++;
				break;
			
			case 'l':
			case 'L':
				printf("Matheus: pera ae meu chegado, eu ainda não sei mecher com arquivos\n");
				printf("\t(o.o)\n");
				prossiga();
				exit++;
				break;
			
			default:
				if(cont_decisao == 5){
					printf("Matheus: coitado... o cara é cego\n");
					printf("\n(-.-)\n");
					printf("P: jogar, L: carregar save\n");
					cont_decisao = 0;
				} else {
					printf("Por favor use p para jogar ou l para carregar um save\n");
					cont_decisao++;
				}
	    }
	} 
}
