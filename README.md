# RPG da etec


História (até agora) : o jogador está correndo o risco de ser expulso. O objetivo do grupo é acabar com as evidências na diretoria.

+ Mapa:
	+ Vários mundos ( Cada mundo é uma localização da escola )
	+ cada mundo a dificuldade aumenta
	+ cada professor é um boss e cada mundo é guardado por um boss

+ Estatisticas:
	+ Atq: ataque
	+ Def: Defesa
	+ Hit: chance de acertar
	+ int: inteligência (até agora inútil)

+ Heróis:
	+ alunos
	+ Cada aluno tem uma habilidade e um especial
	+ o grupo cabe no máximo 5

+ Herói:
	+ Heitor: [super  e r e t o] | [Chá de bola]
	+ Arthur: [brigadeiro] | [CAPITALISMO]
	+ victor: [Tanker] | [Fúria Russa | Vitão sem camisa]
	+ Teago : [MORUNGABA] | [MORUNGABA agressive]
	+ Samuel: [Drogas] | [Trap] 
	+ pinga : [cachaça] | [photoshop top]
	+ david : [soneca] | [Controde de projetor]


+ Estado normal:
	+ Heitor : Ataque corpo-a-corpo de longo alcance, HIT baixo (devido a insegurança)
	+ Arthur : Oferece brigadeiros para os membros da party, curando-os (cura 5% e aumenta 1% a cada nível) | é incapaz de atacar
	+ Victor : Defesa alta e ataque alto
	+ Teago  : Teleporta um herói para a dimensão de MORUNGABA heala 2% de vida por turno
	+ Samuel : Joga cigarro nos inimigos ( Poison (pq singaro da cãrse)) | Usa ( ou pode oferecer aos compatriotas) o beck : aumenta o Hit e diminui a defesa
	+ Pinga  : Oferece ou consome Pinga que aumenta o ataque mas diminui o HIT | Arma: Joga garrafa vazia (ranged)
	+ david  : dorme por 3 turnos e todo ataque que ele recebe pode retornar para o inimigo de escolha

+ Especiais:
	+ cada especial dura 3 turnos

	+ Heitor: Cura Aliados
	+ Arthur: Cura o inimigo com brigadeiros e tira permanentemente 5 % do HP total do alvo
	+ Victor: Vitão Sem Camisa pode proteger 1 aliado de qualquer ataque recebendo o dano e a defesa aumenta em 90% porém o individuo sendo protegido não pode usar ataques corpo-a-corpo
	+ Victor: Se ele pode usar o especial e receber uma pinga, ele temporariamente vira russo ( aumenta o atq  em 50% )
	+ Samuel: causa confusão no inimigo e se houver um heitor no grupo, o ataque e o hit de heitor aumenta em 25%, pode atacar em área ( mas não recebe os bônus de ataque)  e a defesa de heitor abaixa em 50%
	+ pinga : pode aumentar um atributo de um heroi temporariamente
	+ Teago : Teleporta um inimigo para a dimensão de MORUNGABA ataca com o dano de atq e deixa o inimigo confuso com a """"beleza"""" de MORUNGABA
