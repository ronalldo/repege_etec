struct personagem {
	int hp;
	int atq;
	int def;
	int hit;
	int inte;
	int lvl;
	int esp;
	int exp;
	int expL; //limite para passar de nivel
	int isDead;
	int id; 
	char nome[25];
	char sprite[5];
};
struct personagem per[7];

void createPer(int hp, int atq, int def , int hit, int lvl, int esp, int exp, int expL, int id, int isDead, char nome[25], char sprite[5]){
	per[id].hp = hp;
	per[id].atq = atq;
	per[id].def = def;
	per[id].hit = hit;
	per[id].lvl = lvl;
	per[id].esp = esp;
	per[id].exp = exp;
	per[id].expL = expL;
	per[id].id = id;
	per[id].isDead = isDead;
	strcpy(per[id].nome, nome);
	strcpy(per[id].sprite, sprite);
}
struct party {
	int membros[5];
	int membrosUp; //membros nao mortos
	int isDefeated;

};

int getHp(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].hp;
		}
	}
}


int getAtq(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].atq;
		}
	}
}

int getDef (int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].def;
		}
	}
}

int getHit(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].hit;
		}
	}
}

int getLvl(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].lvl;
		}
	}
}

int getExp(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].exp;
		}
	}
}

int getExpL(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].expL;
		}
	}
}

int getDead(int id){
	return per[id].isDead;
}

char* getName(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].nome;
		}
	}
}

char* getSprite(int id){
	for(int i = 0; i <= sizeArray(per) - 1; i++){
		if(per[i].id == id){
			return per[i].sprite;
		}
	}
}

void attack(int id_atacante, int id_alvo){
	if ( !(getAtq(id_atacante) < getDef(id_alvo)) ){
		per[id_alvo].hp = per[id_alvo].hp - (per[id_atacante].atq - per[id_alvo].def);		
	}
}

void battle(struct party amigos, struct party inimigos){
	char acaoA; 
	int alvoA, acaoI, alvoI, heroi, end;
	
	while(amigos.isDefeated == 0 && inimigos.isDefeated == 0){
		end = 0;
		printf("AMIGOS=======================\n");
		for(int i = 0; i < sizeArrayN(amigos.membros); i++){
			printf("%s | HP: %d | LV: %d | EXP %d/%d\n", getName(amigos.membros[i]), getHp(amigos.membros[i]), getLvl(amigos.membros[i]), getExp(amigos.membros[i]), getExpL(amigos.membros[i]));	
		}	
		printf("=============================\n");
		printf("INIMIGOS=====================\n");
		for(int i = 0; i < sizeArrayN(inimigos.membros); i++){
			printf("%s | HP: %d | LV: %d\n", getName(inimigos.membros[i]), getHp(inimigos.membros[i]), getLvl(inimigos.membros[i]));	
		}
		printf("=============================\n");
		/*
		clearCanvas();
		int x = 4;
		for(int i = 0; i < 5; i++){
			if(amigos.membros[i] == NULL || getDead(amigos.membros[i]) == 1){
				putCanvas("     ", x, 2);
			} else {
				putCanvas(getSprite(amigos.membros[i]), x, 2);
				x += 5;
			}
		}
		x = 28;
		for(int i = 0; i < 5; i++){
			if(inimigos.membros[i] == NULL || getDead(inimigos.membros[i]) == 1){
				putCanvas("     ", x, 2);
			} else {
				putCanvas(getSprite(inimigos.membros[i]), x, 2);
				x += 5;
			}
		}

		drawCanvas(); */
		while(end == 0){
			int vezes = sizeArrayN(amigos.membros);
			while(vezes != 0){
				printf("Escolha um heroi: ");
				scanf(" %d", &heroi);
				heroi = amigos.membros[heroi];
				if(getDead(heroi) != 0){
					printf("Esse heroi morreu, big F\n");
					continue;
				}
				printf("A: atacar F: acabar turno\n");
				scanf(" %c", &acaoA);
				switch(acaoA){
					case 'a':
					case 'A':
						printf("Escolha o alvo");
						scanf(" %d", &alvoA);
						alvoA = inimigos.membros[alvoA];
						attack(heroi, alvoA);
						//contra-ataque
						attack(alvoA, heroi);
						if(getHp(alvoA) <= 0){
							per[alvoA].isDead++;
							inimigos.membrosUp--;
						}
						if(getHp(heroi) <= 0){
							per[alvoA].isDead++;
							amigos.membrosUp--;
						}
						vezes--;
						break;
					case 'f':
					case 'F':
						vezes = 0;
						break;
					default:
						printf("Comando invalido, use A ou F\n");
						continue;
				}
				end++;
			}
		}
		clearScreen();
		if(amigos.membrosUp == 0){
				amigos.isDefeated++;
				printf("Funcionarios vitoriosos\n");
			} else if (inimigos.membrosUp == 0){
				inimigos.isDefeated++;
				printf("Alunos Vitoriosos\n");
			}	
	}
}
