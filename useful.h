#define sizeArray(x) (sizeof(x) / sizeof(x[0]))

// conta quantos elementos nao nulos existem (eu acho)
int sizeArrayN(int x[]){
	int cont = 0;
	for(int i = 0; i < sizeof &x / sizeof x[0]; i++){
		if(&x[i] != NULL){
			cont++;
		}
	}
	return cont;
}

void clearScreen(){
	#ifdef _WIN32
	system("cls");
	#else
	printf("\e[H\e[2J");
	#endif
}

void prossiga(){
	char enter = 0;
	printf("Pressione Enter para continuar...");
	while(enter != '\n'){ enter=getchar(); }
	clearScreen();
}
