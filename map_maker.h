/*
 * um desenhador de caixas feito por alguem que
 * mal sabe programar em C 
 */
char canvas[80][6];

void clearCanvas(){
	for(int y = 0; y < 6; y++){
		for(int x = 0; x < 80; x++){
			canvas[x][y] = ' ';
		}

	}
}


void putCanvas(char sprite[5], int x, int y){
	for(int i = 0; i < 5; i++){
		if(sprite[i] == NULL){
			canvas[x][y] = ' ';
			continue;
		}

		canvas[x][y] = sprite[i];
		x++;
		// previne x de ser maior do que o limite
		if(x >= 80){
			printf("Matheus: personagem saiu da tela, revisa teu codigo ai meu consagrado\n\t(-.-)");
			break;
		}
	}
	
}

void drawCanvas(){
	for(int y = 0; y < 6; y++){
		for(int x = 0; x < 80; x++){
			printf("%c", canvas[x][y]);
		}
		printf("\n");
	}
	printf("\n");
}
